template<typename T>
std::string Chip8::toHex(T num) {
    std::stringstream ss;
    ss << std::hex << num;

    std::string zeros;
    std::size_t modulo = ss.str().size() % 4;

    if(modulo != 0) {
        for(std::size_t i = 0; i < 4 - modulo; i++) {
            zeros += "0";
        }
    }

    return "0x" + zeros + ss.str();
}
