#ifndef CHIP8_HPP
#define CHIP8_HPP

// STL includes
#include <cstdint>
#include <string>
#include <vector>
#include <sstream>

class Chip8 {
    public:
        // Runs the emulator
        void run(const std::string &romFilename, float frequency, bool shiftQuirks, bool loadQuirks, bool extendedScreenMode);

    private:
        static std::vector<std::uint8_t> fontset;
        static std::vector<std::uint8_t> bigFontset;

        // Initializes the emulator
        void init();

        // Loads the rom into memory
        void loadRom();

        // Runs the mainloop
        void runMainloop();

        // Executes an opcode and increments the instruction pointer
        bool executeOpcode();

        // Creates a sound
        std::vector<std::int16_t> generateRawSound(float soundFrequency = 440.0f, std::uint32_t sampleRate = 44100, const std::string &waveForm = "square");

        // Number to hexadecimal string convertion for debugging
        template<typename T>
        std::string toHex(T num);

        std::string romFilename;                // Rom filename
        float frequency;                        // Instruction execution frequency
        bool shiftQuirks;                       // Bit shifting quirks
        bool loadQuirks;                        // Register storing quirks
        bool extendedScreenMode;                // Super Chip-8 extended screen mode

        // Member variables
        std::uint16_t ip;                       // Instruction pointer/program counter
        std::uint16_t sp;                       // Stack pointer
        std::uint16_t I;                        // Memory address register
        std::uint8_t dt;                        // Delay timer
        std::uint8_t st;                        // Sound timer
        std::vector<std::uint8_t> v;            // General purpose registers
        std::vector<std::uint8_t> memory;       // 4 kilobytes of memory
        std::vector<std::uint16_t> stack;       // 16 * 16 bits of stack
        std::vector<std::uint8_t> flags;        // Super Chip-8 Flag registers
        std::vector<std::uint8_t> display;      // Display
        std::vector<bool> keys;                 // Key flags
        bool draw;                              // Draw flag
};

#include "Chip8.inl"

#endif // CHIP8_HPP
