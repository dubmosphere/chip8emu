// Chip8 includes
#include "Chip8.hpp"

// STL includes
#include <iostream>

int main(int argc, char **argv)
{
    if(argc > 1) {
        Chip8 chip8;
        float frequency = 500.f;
        bool shiftQuirks = false;
        bool loadQuirks = false;
        bool extendedScreenMode = false;

        if(argc > 2) {
            for(int i = 2; i < argc; i++) {
                std::string arg = std::string(argv[i]);

                if(arg == "-h" || arg == "-f") {
                    if(i + 1 < argc) {
                        frequency = std::stof(argv[i + 1]);
                        i++;
                    }
                } else if(arg == "-q") {
                    if(i + 1 < argc) {
                        std::string arg1 = std::string(argv[i + 1]);

                        if(arg1 == "shift" || arg1 =="s") {
                            shiftQuirks = true;
                            i++;
                        } else if(arg1 == "load" || arg1 =="l") {
                            loadQuirks = true;
                            i++;
                        } else {
                            shiftQuirks = true;
                            loadQuirks = true;
                        }

                        if(i + 2 < argc) {
                            std::string arg2 = std::string(argv[i + 2]);

                            if(arg2 == "shift") {
                                shiftQuirks = true;
                                i++;
                            } else if(arg2 == "load") {
                                loadQuirks = true;
                                i++;
                            }
                        }
                    }
                } else if(arg =="-qs" || arg == "-qshift") {
                    shiftQuirks = true;
                } else if(arg =="-ql" || arg == "-qload") {
                    loadQuirks = true;
                } else if(arg =="-qsl" || arg =="-qls" || arg == "-qshiftload" || arg == "-qloadshift" || arg == "-qall") {
                    shiftQuirks = true;
                    loadQuirks = true;
                } else if(arg == "-esm" || arg == "-e") {
                    extendedScreenMode = true;
                } else {
                    std::cerr << "No such option: " << arg << std::endl;
                }
            }
        }

        chip8.run(std::string(argv[1]), frequency, shiftQuirks, loadQuirks, extendedScreenMode);
    } else {
        std::cerr << "No ROM file given" << std::endl;
    }

    return 0;
}
