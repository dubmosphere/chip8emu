﻿// Class includes
#include "Chip8.hpp"

// SFML includes
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

// STL includes
#include <iostream>
#include <fstream>
#include <ctime>
#include <random>

// Define instructions (0xn000)
#define SYS         0x0
#define JMP         0x1
#define CALL        0x2
#define SE          0x3
#define SNE         0x4
#define SEV         0x5
#define MOV         0x6
#define ADD         0x7
#define OPV         0x8
#define VSNE        0x9
#define MOVI        0xa
#define JV0         0xb
#define RND         0xc
#define DRW         0xd
#define KBD         0xe
#define UT          0xf

// 0x00nn instructions (SYS)
#define CLS         0xe0
#define RET         0xee

// 0x8xyn instructions (VOP)
#define MOVV        0x0
#define OR          0x1
#define AND         0x2
#define XOR         0x3
#define ADDV        0x4
#define SUBV        0x5
#define SHR         0x6
#define DIFV        0x7
#define SHL         0xe

// 0xexnn-instructions (KBD)
#define SEK         0x9e
#define SNEK        0xa1

// 0xfxnn-instructions (UT)
#define LDDT        0x07
#define LDK         0x0a
#define STORDT      0x15
#define STORST      0x18
#define ADDI        0x1e
#define MOVIS       0x29
#define BCD         0x33
#define STOR        0x55
#define LDA         0x65

// 0x00nn instructions (Super Chip-8) (SYS)
#define SCD         0xc
#define SCR         0xfb
#define SCL         0xfc
#define EXIT        0xfd
#define LOW         0xfe
#define HIGH        0xff

// 0xdxyn instructions (Super Chip-8) (UT)
#define XDRW        0x0

// 0xfxnn instructions (Super Chip-8) (UT)
#define MOVISX      0x30
#define STORF       0x75
#define LDAF        0x85

// Define double PI for sine waveform (sound)
#define TWO_PI      6.28318530718f

// Chip8 8*5 pixel fontset
std::vector<std::uint8_t> Chip8::fontset = {
    0xF0, 0x90, 0x90, 0x90, 0xF0, //0
    0x20, 0x60, 0x20, 0x20, 0x70, //1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, //2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, //3
    0x90, 0x90, 0xF0, 0x10, 0x10, //4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, //5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, //6
    0xF0, 0x10, 0x20, 0x40, 0x40, //7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, //8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, //9
    0xF0, 0x90, 0xF0, 0x90, 0x90, //A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, //B
    0xF0, 0x80, 0x80, 0x80, 0xF0, //C
    0xE0, 0x90, 0x90, 0x90, 0xE0, //D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, //E
    0xF0, 0x80, 0xF0, 0x80, 0x80  //F
};

// Chip8 8*10 pixel fontset
std::vector<std::uint8_t> Chip8::bigFontset = {
    0x00, 0x3C, 0x42, 0x42, 0x42, 0x42, 0x42, 0x42, 0x3C, 0x00, // 0
    0x00, 0x08, 0x38, 0x08, 0x08, 0x08, 0x08, 0x08, 0x3E, 0x00, // 1
    0x00, 0x38, 0x44, 0x04, 0x08, 0x10, 0x20, 0x44, 0x7C, 0x00, // 2
    0x00, 0x38, 0x44, 0x04, 0x18, 0x04, 0x04, 0x44, 0x38, 0x00, // 3
    0x00, 0x0C, 0x14, 0x24, 0x24, 0x7E, 0x04, 0x04, 0x0E, 0x00, // 4
    0x00, 0x3E, 0x20, 0x20, 0x3C, 0x02, 0x02, 0x42, 0x3C, 0x00, // 5
    0x00, 0x0E, 0x10, 0x20, 0x3C, 0x22, 0x22, 0x22, 0x1C, 0x00, // 6
    0x00, 0x7E, 0x42, 0x02, 0x04, 0x04, 0x08, 0x08, 0x08, 0x00, // 7
    0x00, 0x3C, 0x42, 0x42, 0x3C, 0x42, 0x42, 0x42, 0x3C, 0x00, // 8
    0x00, 0x3C, 0x42, 0x42, 0x42, 0x3E, 0x02, 0x04, 0x78, 0x00, // 9
    0x00, 0x18, 0x08, 0x14, 0x14, 0x14, 0x1C, 0x22, 0x77, 0x00, // A
    0x00, 0x7C, 0x22, 0x22, 0x3C, 0x22, 0x22, 0x22, 0x7C, 0x00, // B
    0x00, 0x1E, 0x22, 0x40, 0x40, 0x40, 0x40, 0x22, 0x1C, 0x00, // C
    0x00, 0x78, 0x24, 0x22, 0x22, 0x22, 0x22, 0x24, 0x78, 0x00, // D
    0x00, 0x7E, 0x22, 0x28, 0x38, 0x28, 0x20, 0x22, 0x7E, 0x00, // E
    0x00, 0x7E, 0x22, 0x28, 0x38, 0x28, 0x20, 0x20, 0x70, 0x00  // F
};

void Chip8::run(const std::string &romFilename, float frequency, bool shiftQuirks, bool loadQuirks, bool extendedScreenMode)
{
    this->romFilename = romFilename;
    this->frequency = frequency;
    this->shiftQuirks = shiftQuirks;
    this->loadQuirks = loadQuirks;
    this->extendedScreenMode = extendedScreenMode;

    init();
    loadRom();
    runMainloop();
}

void Chip8::init()
{
    ip = 0x200;
    sp = 0;
    I = 0x000;
    dt = 0x00;
    st = 0x00;

    keys.clear();
    display.clear();
    flags.clear();
    stack.clear();
    memory.clear();
    v.clear();

    v.resize(16);
    memory.resize(0x1000);
    stack.resize(16);
    flags.resize(8);
    display.resize(128 * 64);
    keys.resize(16);

    draw = true;

    srand(static_cast<std::uint32_t>(time(NULL)));

    for(std::size_t i = 0; i < fontset.size(); i++) {
        memory.at(i) = fontset.at(i);
    }
    for(std::size_t i = 0; i < bigFontset.size(); i++) {
        memory.at(fontset.size() + i) = bigFontset.at(i);
    }
}

void Chip8::loadRom()
{
    std::ifstream rom(romFilename, std::ios::binary);
    std::size_t i = 0;

    while(rom.good()) {
        std::int32_t c = rom.get();
        if(c > -1) {
            memory.at(ip + i++) = c & 0x00ff;
        }
    }

    rom.close();
}

void Chip8::runMainloop()
{
    std::float_t pixelScale = 8.f;

    sf::RenderWindow window(sf::VideoMode(static_cast<std::uint32_t>(128 * pixelScale), static_cast<std::uint32_t>(64 * pixelScale)), "Chip-8", sf::Style::Close);
    sf::Color backgroundColor(0x14, 0x14, 0x14);

    sf::RectangleShape pixel(sf::Vector2f(pixelScale, pixelScale));
    pixel.setFillColor(sf::Color(0xf8, 0x67, 0x0e));

    std::uint32_t sampleRate = 44100;
    std::vector<std::int16_t> rawSound = generateRawSound(220.0f, sampleRate, "square");

    sf::SoundBuffer soundBuffer;
    if(!soundBuffer.loadFromSamples(rawSound.data(), sampleRate, 1, sampleRate)) {
        throw "Chip8::mainloop() error: Error loading sound sample";
    }

    sf::Sound sound;
    sound.setBuffer(soundBuffer);
    sound.setLoop(true);
    sound.setVolume(10.0f);

    sf::Clock clock;
    sf::Time tpf = sf::seconds(1.f / this->frequency);
    sf::Time accumulator = sf::Time::Zero;
    sf::Time timerTpf = sf::seconds(1.f / 60.f);
    sf::Time timerAccumulator = sf::Time::Zero;

    while (window.isOpen()) {
        sf::Event event;
        while(window.pollEvent(event)) {
            if(event.type == sf::Event::Closed) {
                window.close();
            }

            if(event.type == sf::Event::KeyPressed) {
                if(event.key.code == sf::Keyboard::Escape) {
                    window.close();
                }
                if(event.key.code == sf::Keyboard::F1) {
                    init();
                    loadRom();
                }

                keys.at(0x1) = event.key.code == sf::Keyboard::Num1;
                keys.at(0x2) = event.key.code == sf::Keyboard::Num2;
                keys.at(0x3) = event.key.code == sf::Keyboard::Num3;
                keys.at(0xc) = event.key.code == sf::Keyboard::Num4;
                keys.at(0x4) = event.key.code == sf::Keyboard::Q;
                keys.at(0x5) = event.key.code == sf::Keyboard::W;
                keys.at(0x6) = event.key.code == sf::Keyboard::E;
                keys.at(0xd) = event.key.code == sf::Keyboard::R;
                keys.at(0x7) = event.key.code == sf::Keyboard::A;
                keys.at(0x8) = event.key.code == sf::Keyboard::S;
                keys.at(0x9) = event.key.code == sf::Keyboard::D;
                keys.at(0xe) = event.key.code == sf::Keyboard::F;
                keys.at(0xa) = event.key.code == sf::Keyboard::Y;
                keys.at(0x0) = event.key.code == sf::Keyboard::X;
                keys.at(0xb) = event.key.code == sf::Keyboard::C;
                keys.at(0xf) = event.key.code == sf::Keyboard::V;
            }
            if(event.type == sf::Event::KeyReleased) {
                keys.at(0x1) = keys.at(0x1) && !(event.key.code == sf::Keyboard::Num1);
                keys.at(0x2) = keys.at(0x2) && !(event.key.code == sf::Keyboard::Num2);
                keys.at(0x3) = keys.at(0x3) && !(event.key.code == sf::Keyboard::Num3);
                keys.at(0xc) = keys.at(0xc) && !(event.key.code == sf::Keyboard::Num4);
                keys.at(0x4) = keys.at(0x4) && !(event.key.code == sf::Keyboard::Q);
                keys.at(0x5) = keys.at(0x5) && !(event.key.code == sf::Keyboard::W);
                keys.at(0x6) = keys.at(0x6) && !(event.key.code == sf::Keyboard::E);
                keys.at(0xd) = keys.at(0xd) && !(event.key.code == sf::Keyboard::R);
                keys.at(0x7) = keys.at(0x7) && !(event.key.code == sf::Keyboard::A);
                keys.at(0x8) = keys.at(0x8) && !(event.key.code == sf::Keyboard::S);
                keys.at(0x9) = keys.at(0x9) && !(event.key.code == sf::Keyboard::D);
                keys.at(0xe) = keys.at(0xe) && !(event.key.code == sf::Keyboard::F);
                keys.at(0xa) = keys.at(0xa) && !(event.key.code == sf::Keyboard::Y);
                keys.at(0x0) = keys.at(0x0) && !(event.key.code == sf::Keyboard::X);
                keys.at(0xb) = keys.at(0xb) && !(event.key.code == sf::Keyboard::C);
                keys.at(0xf) = keys.at(0xf) && !(event.key.code == sf::Keyboard::V);
            }
        }

        if(accumulator >= tpf) {
            try {
                if(!executeOpcode()) {
                    window.close();
                }
            } catch(const std::string &msg) {
                std::cerr << msg << std::endl;
            }

            accumulator = sf::Time::Zero;
        }

        if(timerAccumulator >= timerTpf) {
            if(dt > 0) {
                dt--;
            }
            if(st > 0) {
                if(sound.getStatus() != sf::Sound::Playing) {
                    sound.play();
                }
                st--;
            } else {
                if(sound.getStatus() == sf::Sound::Playing) {
                    sound.stop();
                }
            }

            timerAccumulator = sf::Time::Zero;
        }

        if(draw) {
            window.clear(backgroundColor);

            for(std::size_t i = 0; i < 64; i++) {
                for(std::size_t j = 0; j < 128; j++) {
                    if(display.at(j + i * 128) == 1) {
                        /*std::uint8_t r = rand() % 255;
                        std::uint8_t g = rand() % 255;
                        std::uint8_t b = rand() % 255;
                        pixel.setFillColor(sf::Color(r, g, b));*/
                        pixel.setPosition(pixelScale * j, pixelScale * i);
                        window.draw(pixel);
                    }
                }
            }

            window.display();

            draw = false;
        }

        accumulator += clock.getElapsedTime();
        timerAccumulator += clock.restart();
    }
}

bool Chip8::executeOpcode() {
    if(ip < memory.size() - 1) {
        std::uint16_t opcode = static_cast<std::uint16_t>(memory.at(ip) << 8) | static_cast<std::uint16_t>(memory.at(ip + 1));
        std::uint8_t x = (opcode & 0x0f00) >> 8;
        std::uint8_t y = (opcode & 0x00f0) >> 4;
        std::uint16_t nnn = opcode & 0x0fff;
        std::uint8_t nn = opcode & 0x00ff;
        std::uint8_t n = opcode & 0x000f;

        ip += 2;

        switch(opcode >> 12) {
            case SYS:   // System instructions beginning with, 0000 => SYS addr (Unused so we can do this like that (ugly xD))
                switch(nn) {
                    case CLS:           // Clears the screen, 00e0 => CLS
                        for(std::size_t i = 0; i < display.size(); i++) {
                            display.at(i) = 0;
                        }
                        draw = true;
                        break;
                    case RET:           // Returns from sub routine, 00ee => RET
                        ip = stack.at(--sp);
                        break;
                    case SCR:           // Scroll 4 pixels to the right, 0x00fb
                        {
                            std::vector<std::uint8_t> displayCopy(display.size());
                            for(std::size_t i = 0; i < 64; i++) {
                                for(std::size_t j = 127; j > 3; j--) {
                                    displayCopy.at(j + i * 128) = display.at((j - 4) + i * 128);
                                }
                            }
                            display = displayCopy;
                        }
                        break;
                    case SCL:           // Scroll 4 pixels to the left, 0x00fc
                        {
                            std::vector<std::uint8_t> displayCopy(display.size());
                            for(std::size_t i = 0; i < 64; i++) {
                                for(std::size_t j = 0; j < 124; j++) {
                                    displayCopy.at(j + i * 128) = display.at((j + 4) + i * 128);
                                }
                            }
                            display = displayCopy;
                        }
                        break;
                    case EXIT:          // Exit emulator, 0x00fd
                        return false;
                    case LOW:           // Deactivate 128x64 pixel mode, 0x00fe
                        extendedScreenMode = false;
                        break;
                    case HIGH:          // Activate 128x64 pixel mode, 0x00ff
                        extendedScreenMode = true;
                        break;
                    default:
                        switch(y) {
                            case SCD:   // Scroll a line down, 0x00c0
                                {
                                    std::vector<std::uint8_t> displayCopy(display.size());
                                    for(std::size_t i = 63; i > n; i--) {
                                        for(std::size_t j = 0; j < 128; j++) {
                                            displayCopy.at(j + i * 128) = display.at(j + (i - n) * 128);
                                        }
                                    }
                                    display = displayCopy;
                                }
                                break;
                            default:
                                throw "Opcode not found: " + toHex(opcode);
                        }
                        break;
                }
                break;
            case JMP:   // Jump to address nnn, 1nnn => JP addr
                ip = nnn;
                break;
            case CALL:  // Call address nnn and push ip onto stack, 2nnn => CALL addr
                stack.at(sp++) = ip;
                ip = nnn;
                break;
            case SE:    // Skip next instruction if vx equals nn, 3xnn => SE Vx, byte
                if(v.at(x) == nn) {
                    ip += 2;
                }
                break;
            case SNE:   // Skip next instruction if vx does not equal nn, 4xnn => SNE Vx, nn
                if(v.at(x) != nn) {
                    ip += 2;
                }
                break;
            case SEV:   // Skip next instruction if vx equals vy, 5xy0 => SE Vx, Vy
                if(v.at(x) == v.at(y)) {
                    ip += 2;
                }
                break;
            case MOV:   // Set vx to nn, 6xnn => LD Vx, byte
                v.at(x) = nn;
                break;
            case ADD:   // Add nn to vx, 7xnn => ADD Vx, byte
                v.at(x) += nn;
                break;
            case OPV:   // V-Register operations, 8000
                switch(n) {
                    case MOVV:  // Load vy into vx, 8xy0 => LD Vx, Vy
                        v.at(x) = v.at(y);
                        break;
                    case OR:    // Bitwise or vx with vy, 8xy1 => OR Vx, Vy
                        v.at(x) |= v.at(y);
                        break;
                    case AND:   // Bitwise and vx with vy, 8xy2 => AND Vx, Vy
                        v.at(x) &= v.at(y);
                        break;
                    case XOR:   // Bitwise xor vx with vy, 8xy3 => XOR Vx, Vy
                        v.at(x) ^= v.at(y);
                        break;
                    case ADDV:  // Add vy to vx, 8xy4 => ADD Vx, Vy
                        if(v.at(x) + v.at(y) > UINT8_MAX) {
                            v.at(0xf) = 1;
                        } else {
                            v.at(0xf) = 0;
                        }

                        v.at(x) += v.at(y) & 0xff;
                        break;
                    case SUBV:  // Subtract vy from vx and store in vx, 8xy5 => SUB Vx, Vy
                        if(v.at(x) > v.at(y)) {
                            v.at(0xf) = 1;
                        } else {
                            v.at(0xf) = 0;
                        }

                        v.at(x) -= v.at(y);
                        break;
                    case SHR:  // Shift vx to the right, 8xy6 => SHR Vx {, Vy}
                        if(v.at(x) & 0x1) {
                            v.at(0xf) = 1;
                        } else {
                            v.at(0xf) = 0;
                        }

                        if(shiftQuirks) {
                            v.at(x) /= 2;
                        } else {
                            v.at(x) = v.at(y) / 2;
                        }
                        break;
                    case DIFV: // Subtract vx from vy and store in vx, 8xy7 => SUBN Vx, Vy
                        if(v.at(y) > v.at(x)) {
                            v.at(0xf) = 1;
                        } else {
                            v.at(0xf) = 0;
                        }

                        v.at(x) = v.at(y) - v.at(x);
                        break;
                    case SHL:  // Shift vx to the left, 8xye => SHL Vx {, Vy}
                        if(v.at(x) & 0x80) {
                            v.at(0xf) = 1;
                        } else {
                            v.at(0xf) = 0;
                        }

                        if(shiftQuirks) {
                            v.at(x) *= 2;
                        } else {
                            v.at(x) = v.at(y) * 2;
                        }
                        break;
                    default:
                        throw "Opcode not found: " + toHex(opcode);
                }
                break;
            case VSNE:  // Skip next instruction if vx does not equal vy, 9xy0 => SNE Vx, Vy
                if(v.at(x) != v.at(y)) {
                    ip += 2;
                }
                break;
            case MOVI:  // Set I to address nnn, annn => LD I, addr
                I = nnn;
                break;
            case JV0:   // Set ip to nnn + v0, bnnn => JP V0, nnn
                ip = nnn + v.at(0x0);
                break;
            case RND:   // Generate random number between 0 - 255, cnnn => RND Vx, byte
                v.at(x) = (rand() % 255) & nn;
                break;
            case DRW:   // Draw to the screen, dxyn => DRW Vx, Vy, n
                {
                    std::size_t pixelScale = 1;

                    v.at(0xf) = 0;
                    if(n == XDRW && extendedScreenMode) {
                        n = 32;

                        for(std::size_t i = 0; i < n; i += 2) {
                            std::size_t iy = ((v.at(y) + (i / 2)) * pixelScale) % 64;

                            for(std::size_t j = 0; j < 2; j++) {
                                std::uint8_t pixel = memory.at(I + i + j);

                                for(std::size_t k = 0; k < 8; k++) {
                                    if((pixel & (0x80 >> k)) != 0) {
                                        std::size_t ix = ((v.at(x) + k) * pixelScale) % 128;
                                        std::uint8_t &displayPixel = display.at((ix + (8 * j)) + iy * 128);

                                        if(displayPixel == 1) {
                                            v.at(0xf) = 1;
                                        }

                                        displayPixel ^= 1;
                                    }
                                }
                            }
                        }
                    } else {
                        if(n == XDRW) {
                            n = 16;
                        }

                        if(!extendedScreenMode) {
                            pixelScale = 2;
                        }

                        for(std::size_t i = 0; i < n; i++) {
                            std::uint8_t pixel = memory.at(I + i);
                            std::size_t iy = ((v.at(y) + i) * pixelScale) % 64;

                            for(std::size_t j = 0; j < 8; j++) {
                                if((pixel & (0x80 >> j)) != 0) {
                                    std::size_t ix = ((v.at(x) + j) * pixelScale) % 128;

                                    for(std::size_t k = 0; k < pixelScale; k++) {
                                        for(std::size_t l = 0; l < pixelScale; l++) {
                                            std::uint8_t &displayPixel = display.at((ix + l) + (iy + k) * 128);

                                            if(displayPixel == 1) {
                                                v.at(0xf) = 1;
                                            }

                                            displayPixel ^= 1;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    draw = true;
                }
                break;
            case KBD:   // Keyboard input instructions, e000
                switch(nn) {
                    case SEK:   // Skip next instruction if key vx is pressed SKP Vx, ex9e
                        if(keys.at(v.at(x))) {
                            ip += 2;
                        }
                        break;
                    case SNEK:  // Skip next instruction if key vx is not pressed SKNP Vx, exa1
                        if(!keys.at(v.at(x))) {
                            ip += 2;
                        }
                        break;
                    default:
                        throw "Opcode not found: " + toHex(opcode);
                }
                break;
            case UT:    // Utility instructions, f000
                switch(nn) {
                    case LDDT:      // Set vx to dt, fx07 => LD Vx, DT
                        v.at(x) = dt;
                        break;
                    case LDK:       // Wait for keyboard input, fx0a => LD Vx, K
                        {
                            bool keyPressed = false;
                            for(std::size_t i = 0; i < 16; i++) {
                                if(keys.at(i)) {
                                    keyPressed = true;
                                    v.at(x) = i & 0x00ff;
                                    break;
                                }
                            }

                            // Repeat instruction if no key is pressed
                            if(!keyPressed) {
                                ip -= 2;
                            }
                        }
                        break;
                    case STORDT:    // Set dt to vx, fx15 => LD DT, Vx
                        dt = v.at(x);
                        break;
                    case STORST:    // Set st to ld, fx18 => LD ST, Vx
                        st = v.at(x);
                        break;
                    case ADDI:      // Add vx to I, fx1e => ADD I, Vx
                        I += v.at(x);
                        break;
                    case MOVIS:     // Set I to sprite location to load 5 byte sprite vx, fx29 => LD F, Vx
                        I = v.at(x) * 5;
                        break;
                    case BCD:       // Store BCD representation of vx in addresses I, I+1 and I+2, fx33 => LD B, Vx
                        memory.at(I) = v.at(x) / 100;
                        memory.at(I + 1) = (v.at(x) / 10) % 10;
                        memory.at(I + 2) = (v.at(x) % 100) % 10;
                        break;
                    case STOR:      // Store registers v0 to vx to address I (Sprite address), fx55 => LD [I], Vx
                        for(std::uint16_t i = 0; i <= x; i++) {
                            memory.at(I + i) = v.at(i);
                        }

                        if(!loadQuirks) {
                            I += x + 1;
                        }
                        break;
                    case LDA:       // Read registers v0 to vx from address I, fx65 => LD Vx, [I]
                        for(std::size_t i = 0; i <= x; i++) {
                            v.at(i) = memory.at(I + i);
                        }

                        if(!loadQuirks) {
                            I += x + 1;
                        }
                        break;
                    case MOVISX:      // Set I to sprite location to load 10 byte sprite, 0xfx30, LD F, Vx
                        I = v.at(x) * 10 + static_cast<std::uint16_t>(fontset.size());
                        break;
                    case STORF:    // Store v0 to vx into flags registers, 0xfx75
                        for(std::size_t i = 0; i <= x; i++) {
                            flags.at(i) = v.at(i);
                        }
                        break;
                    case LDAF:     // Load flags registers into v0 to vx, 0xfx85
                        for(std::size_t i = 0; i <= x; i++) {
                            v.at(i) = flags.at(i);
                        }
                        break;
                    default:
                        throw "Opcode not found: " + toHex(opcode);
                }
                break;
            default:
                throw "Opcode not found: " + toHex(opcode);
        }
        return true;
    }

    return false;
}

std::vector<std::int16_t> Chip8::generateRawSound(float soundFrequency, std::uint32_t sampleRate, const std::string &waveForm)
{
    std::vector<std::int16_t> rawSound(sampleRate);
    float amp = 0.9f;

    for (std::size_t i = 0; i < rawSound.size(); i++) {
        float time = static_cast<float>(i);
        std::int16_t out = 0;
        std::int16_t amplitude = static_cast<std::int16_t>(32767.f * amp);

        if(waveForm == "square") {
            std::int32_t tpc = static_cast<std::int32_t>(sampleRate / soundFrequency);
            std::int32_t cyclepart = static_cast<std::int32_t>(time) % tpc;
            std::int32_t halfcycle = tpc / 2;
            if(cyclepart < halfcycle) {
                out = amplitude;
            }
        } else if(waveForm == "sinus" || waveForm == "sine") {
            float tpc = sampleRate / soundFrequency;
            float cycles = time / tpc;
            float rad = TWO_PI * cycles;
            out = static_cast<std::int16_t>(amplitude * std::sin(rad));
        } else if(waveForm == "triangle" || waveForm == "tri") {
            float tpc = sampleRate / soundFrequency;
            float cycles = time / tpc;
            out = static_cast<std::int16_t>(amplitude * cycles);
        }

        rawSound.at(i) = out;
    }

    return rawSound;
}
