# Superchip-8 Interpreter/Emulator #
This is a very simple Superchip-8 interpreter/emulator written in C++ using SFML for displaying stuff and outputting audio. I know there are many Superchip/Chip-8 interpreters out there but I wanted to learn something new and was interested in emulators. So now I share my result. It still has some bugs with some Superchip-8 games.

## Compiling ##
This program uses SFML for creating a window, rendering to it and producing audio. Information on how to get/build SFML can be found [here](https://www.sfml-dev.org/).

You can compile the Chip-8 interpreter with help of CMake. This should be pretty clear.

### Linux ###

```
#!bash

cd path/to/repo
mkdir build
cd build
cmake ..
```

... or with cmake-gui

### Windows ###

Use cmake-gui to generate Visual Studio Project, open it with Visual Studio and build it.

## Usage ##

Here is how you run the interpreter. With -esm you can force the emulator to start with extended screen mode (128x64px, this is not needed most of the time in Superchip-8 games):


```
#!bash

./chip8emu ROM [[-q [quirk]] [-h frequency] [-esm]]
```

### Quirks ###

With the quirk option you can activate some quirks some newer implementations of Chip-8 had. This is to support some games programmed for those implementations. If only -q is given all quirks are applied.

```
#!bash


-q [quirk1] [quirk2]
```


#### Available Options ####

* shift: Makes bit shift operation ignore vy register
* load: Makes load/store ignore I register

### Frequency ###

The frequency option sets the amount of instructions per second so you can speed up games with this option


```
#!bash

./chip8emu ROM -h frequency

```
... or ...

```
#!bash

./chip8emu ROM -f frequency

```

### Examples ###

For the game BLINKY you need for example the quirks shift and load, so the command would look like this (With a Instruction frequency of 1000 hertz):

```
#!bash

./chip8emu BLINKY -q shift load -h 1000
```

Or if you only need one quirk:


```
#!bash

./chip8emu ROM -q shift -h 1000
```

### Input ###
So the keyboard-layout of the original Chip-8 looks as following:


```
#!

1 | 2 | 3 | C
4 | 5 | 6 | D
7 | 8 | 9 | E
A | 0 | B | F
```


I mapped it to that (Using a qwertz keyboard):


```
#!

1 | 2 | 3 | 4
Q | W | E | R
A | S | D | F
Y | X | C | V
```

## Future plans ##

* Keyboard configuration

## Thanks to ##
* [Thomas P. Greene](http://devernay.free.fr/hacks/chip8/C8TECH10.HTM) For this absolutely great specification of the Chip-8 architecture
* [tomdaley92](https://github.com/tomdaley92/Chip8/issues/9) For understanding what games are using the "wrongly" implemented chip-8 versions